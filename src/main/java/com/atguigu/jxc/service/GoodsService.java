package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    /**
     * 首页：当前库存查询
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 查询所有商品信息（可以根据分类、名称查询）
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     * 修改或添加商品
     * @param goods
     * @return
     */
    ServiceVO save(Goods goods);

    /**
     * 删除指定商品
     * @param goodsId
     */
    ServiceVO delete(Integer goodsId);

    /**
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getNoInventory(Integer page, Integer rows, String nameOrCode);
    /**
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getHasInventory(Integer page, Integer rows, String nameOrCode);

    /**
     *添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    /**
     * 删除库存
     * @param goodsId
     * @return
     */
    ServiceVO deleteStock(Integer goodsId);

    /**
     * 库存预警
     * @return
     */
    Map<String, Object> listAlarm();
}

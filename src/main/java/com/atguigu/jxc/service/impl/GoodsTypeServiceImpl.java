package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public  ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    @Override
    public Integer getGoodTypeStaById(Integer goodsTypeId) {
        if (goodsTypeId != null){
            Integer i = goodsTypeDao.getTypeState(goodsTypeId);
            return i;
        }else {
            return -1;
        }
    }

    @Override
    public List<Integer> getGoodsTypeIdByPid(Integer goodsTypeId) {
        return null;
    }


    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {

        GoodsType goodsType = goodsTypeDao.checkGoodsTypeSta(pId);
        if (goodsType.getGoodsTypeState() == 1){
            //如果是1说明是父亲节点，我就直接在父亲节点下新增状态为叶子结点的分类
            goodsTypeDao.addCategory(goodsTypeName,pId);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        //否则说明是叶子结点，在叶子结点下新增分类，需要将此叶子结点状态更新为1后，再新增新的叶子结点分类
        goodsTypeDao.setGoodsTypeSta(pId);
        goodsTypeDao.addCategory(goodsTypeName,pId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    /**
     * 删除商品分类
     * @param goodsTypeId
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        //1.删除前检查节点类型2.父节点下无叶子结点，可删除/叶子结点下无商品可删除
        GoodsType goodsType = goodsTypeDao.checkGoodsTypeSta(goodsTypeId);
        List<GoodsType> list = goodsTypeDao.getGoodsChildrenTypeId(goodsTypeId);

        //List<Goods> goodsList = goodsDao.getGoodList(goodsTypeId);
        List<Goods> goodsList = goodsDao.getGoodListByTypeId(goodsTypeId);

        if (goodsType.getGoodsTypeState() == 1 && CollectionUtils.isEmpty(list)){
            goodsTypeDao.delete(goodsTypeId);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        //if (goodsType.getGoodsTypeState() == 0 && CollectionUtils.isEmpty(goodsList))
        if (goodsType.getGoodsTypeState() == 0 && goodsList.isEmpty()) {
            Integer pId = goodsType.getPId();
            goodsTypeDao.delete(goodsTypeId);
            List<GoodsType> restChildrenList = goodsTypeDao.getGoodsChildrenTypeId(pId);
            if (CollectionUtils.isEmpty(restChildrenList)){
                goodsTypeDao.setGoodsTypeSta0(pId);
            }
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO<>(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);

    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId){

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            HashMap obj = (HashMap) array.get(i);

            if(obj.get("state").equals("open")){// 如果是叶子节点，不再递归

            }else{// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId){

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if(goodsType.getGoodsTypeState() == 1){
                obj.put("state", "closed");

            }else{
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}

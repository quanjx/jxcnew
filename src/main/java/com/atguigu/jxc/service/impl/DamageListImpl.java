package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;
import com.atguigu.jxc.service.UnitService;
import com.atguigu.jxc.service.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class DamageListImpl implements DamageListService {

    @Autowired
    private DamageListDao damageListDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {

        HashMap<String, Object> map = new HashMap<>();

        List<DamageList> list = damageListDao.getListByTime(sTime,eTime);

        map.put("rows",list);
        return map;
    }


    /**
     * 新增报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession httpSession) {

        User currentUser =(User)httpSession.getAttribute("currentUser");
        damageList.setUserId(currentUser.getUserId());
        damageListDao.addDamageList(damageList);

        Integer damageListId = damageList.getDamageListId();

        log.info(damageListGoodsStr);
        List<DamageListGoods> damageListGoodsList = JSON.parseObject(damageListGoodsStr, new TypeReference<List<DamageListGoods>>() {
        });
//        System.out.println("mapListmaps = " + mapListmaps);

        damageListGoodsList.stream().forEach(damageListGoods -> {

//            Integer goodsTypeId = (Integer) objectMap.get("goodsTypeId");
//            System.out.println("goodsTypeId = " + goodsTypeId);
//            DamageListGoods damageListGoods = new DamageListGoods();
//            damageListGoods.setDamageListGoodsId(stringObjectMap.get("goodsTypeId"));
            damageListGoods.setDamageListId(damageListId);
            damageListGoodsDao.insert(damageListGoods);
        });

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {

        HashMap<String, Object> hashMap = new HashMap<>();

        List<DamageListGoods> damageGoodsList = damageListGoodsDao.getDamageGoodsList(damageListId);

        hashMap.put("rows",damageGoodsList);
        return hashMap;
    }

}

package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierManageDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierManageImpl implements SupplierManageService {

    @Autowired
    private SupplierManageDao supplierManageDao;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {

        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> supplierList = supplierManageDao.getSupplierByName(offSet,rows,supplierName);

        map.put("total",supplierManageDao.getSupplierCount());
        map.put("rows",supplierList);
        return map;
    }

    /**
     * 新增/修改供应商信息
     * @return
     */
    @Override
    public ServiceVO save(Supplier supplier) {
        //1.如果供应商已存在，则修改
        if(supplier.getSupplierId() != null){
            String supplierName = supplier.getSupplierName();
            String address = supplier.getAddress();
            String remarks = supplier.getRemarks();
            String contacts = supplier.getContacts();
            String phoneNumber = supplier.getPhoneNumber();
            Integer supplierId = supplier.getSupplierId();

            Integer i = supplierManageDao.updateById(supplierId, supplierName, address, remarks, contacts, phoneNumber);
            if (i != 0){
                return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
            }
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);

        }
            //2.如果供应商不存在，则新增
            Integer i = supplierManageDao.add(supplier);
            if (i != 0){
                return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
            }
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
    }

    /**
     * 根据ID删除指定供应商
     * @param ids
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {
        Integer i = supplierManageDao.deleteById(ids);
        if (i != 0){
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
    }
}

package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private SaleListGoodsDao saleListGoodsDao;

    @Autowired
    private GoodsTypeService goodsTypeService;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {

        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsListByCnameOrGType(offSet,rows,codeOrName,goodsTypeId);
        System.out.println("goodsList = " + goodsList);
        if (!CollectionUtils.isEmpty(goodsList)){
            goodsList.stream().forEach(goods -> {
                Integer totalByGoodsId = saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId());
                goods.setSaleTotal(totalByGoodsId);
                goods.setGoodsTypeName(goods.getGoodsTypeName());
            });
        }

        map.put("total",goodsDao.getGoodsCount());
        map.put("rows",goodsList);

        return map;
    }

    /**
     * 查询所有商品信息（可以根据分类、名称查询）
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;
            //传参呢  我看看传参

        HashMap<String, Object> map = new HashMap<>();

        //如果前端没给传，后端业务自己增加逻辑处理一下
            if(goodsTypeId != null){
                goodsTypeId =  goodsTypeId == 1 ? 0 :  goodsTypeId;
            }

        List<Goods> goodsList1 = goodsDao.getGoodList(offset,rows,goodsName,goodsTypeId);
        if (!CollectionUtils.isEmpty(goodsList1)){
            goodsList1.stream().forEach(goods -> {
                Integer totalByGoodsId = saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId());
                goods.setSaleTotal(totalByGoodsId);
                goods.setGoodsTypeName(goods.getGoodsTypeName());
            });
        }

        map.put("total",goodsDao.getGoodsCount());
        map.put("rows",goodsList1);
        return map;

    }

    /**
     * 修改或添加商品
     * @param goods
     * @return
     */
    @Override
    public ServiceVO save(Goods goods) {
        if (goods.getGoodsId() != null){
            goodsDao.updateById(goods);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        goodsDao.add(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除指定商品
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsId) {
        goodsDao.deleteById(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoInventory(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        HashMap<String, Object> map = new HashMap<>();
        List<Goods> list = goodsDao.getNoInventoryGoodList(offSet,rows,nameOrCode);

        if (!CollectionUtils.isEmpty(list)){
            list.stream().forEach(goods -> {
                Integer totalByGoodsId = saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId());
                goods.setSaleTotal(totalByGoodsId);
                goods.setGoodsTypeName(goods.getGoodsTypeName());
            });
        }

        map.put("total",goodsDao.getGoodsCount());
        map.put("rows",list);
        return map;
    }


    /**
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasInventory(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        HashMap<String, Object> map = new HashMap<>();
        List<Goods> list = goodsDao.getHasInventoryGoodList(offSet,rows,nameOrCode);

        if (!CollectionUtils.isEmpty(list)){
            list.stream().forEach(goods -> {
                Integer totalByGoodsId = saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId());
                goods.setSaleTotal(totalByGoodsId);
                goods.setGoodsTypeName(goods.getGoodsTypeName());
            });
        }

        map.put("total",goodsDao.getGoodsCount());
        map.put("rows",list);
        return map;
    }

    /**
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveInventoryQuantity(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Integer i = goodsDao.getGoodsState(goodsId);
        if (i == 0){
            goodsDao.updateGoodsStock(goodsId);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO<>(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS);
    }

    /**
     * 库存预警
     */
    @Override
    public Map<String, Object> listAlarm() {

        HashMap<String, Object> hashMap = new HashMap<>();
        List<Goods> list = goodsDao.getaAlarmGoodsList();
        hashMap.put("rows",list);
        return hashMap;
    }

}

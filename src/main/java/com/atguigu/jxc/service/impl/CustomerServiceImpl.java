package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;
    /**
     * 分页展示客户列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customerList = customerDao.getListByName(offSet,rows,customerName);

        map.put("total",customerDao.getCustomerCount());
        map.put("rows",customerList);
        return map;
    }

    /**
     * 客户添加或修改
     * @param customer
     * @return
     */
    @Override
    public ServiceVO save(Customer customer) {
        //如果客户已存在，则修改
        if(customer.getCustomerId() != null){
            Integer i = customerDao.updateById(customer.getCustomerId(),customer);
            log.info("i",i);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else {
            Integer i = customerDao.add(customer);
            log.info("i",i);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }

    }

    @Override
    public ServiceVO delete(String ids) {

        customerDao.deleteById(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}

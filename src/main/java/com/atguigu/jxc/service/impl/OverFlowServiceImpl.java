package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.jxc.dao.OverFlowDao;
import com.atguigu.jxc.dao.OverFlowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverFlowServiceImpl implements OverFlowService {

    @Autowired
    private OverFlowDao overFlowDao;

    @Autowired
    private OverFlowListGoodsDao overFlowListGoodsDao;
    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @param httpSession
     * @return
     */
    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession httpSession) {
        /**
         * 新增t_overflow_list表
         */
        //1.通过session获得用户ID信息
        User currentUser = (User) httpSession.getAttribute("currentUser");
        //2.完善overflowList对象的user_id信息
        overflowList.setUserId(currentUser.getUserId());
        //3.新增t_overflow_list表一条记录
        overFlowDao.addOverflowList(overflowList);
        /**
         * 新增t_overflow_list_goods表
         */
        //4.获取新增的记录回填的主键
        Integer overflowListId = overflowList.getOverflowListId();
        //5.解析String类型的JSON数据为OverflowListGoods类型的list对象
        List<OverflowListGoods> overflowListGoods = JSON.parseObject(overflowListGoodsStr, new TypeReference<List<OverflowListGoods>>(){});
        //6.将自增主键填进t_overflow_list_goods表的对象中
        overflowListGoods.stream().forEach(overflowGoods->{
            overflowGoods.setOverflowListId(overflowListId);
            //7.将完善好的OverflowListGoods插入t_overflow_list_goods表中
            overFlowListGoodsDao.insert(overflowGoods);
        });

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 根据时间查询报溢单列表信息
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> hashMap = new HashMap<>();
        List<OverflowList> list = overFlowDao.getListByTime(sTime,eTime);
        hashMap.put("rows",list);
        return hashMap;
    }

    @Override
    public Map<String, Object> goodList(Integer overflowListId) {
        HashMap<String, Object> hashMap = new HashMap<>();
        List<OverflowListGoods> list = overFlowListGoodsDao.getOverFlowGoodsList(overflowListId);
        hashMap.put("rows",list);
        return hashMap;
    }
}

package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    /**
     * 通过商品类型ID查询商品类型（父节点？子节点）
     * @param goodsTypeId
     * @return
     */
    Integer getGoodTypeStaById(Integer goodsTypeId);

    /**
     * 通过父节点的商品类型ID 找到属于该父节点的子分类
     * @param goodsTypeId
     * @return
     */
    List<Integer> getGoodsTypeIdByPid(Integer goodsTypeId);

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    ServiceVO save(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    ServiceVO delete(Integer goodsTypeId);
}

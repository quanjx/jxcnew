package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    /**
     * 分页展示客户列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);

    /**
     * 客户添加或修改
     * @param customer
     * @return
     */
    ServiceVO save(Customer customer);

    /**
     * 删除客户
     * @param ids
     * @return
     */
    ServiceVO delete(String ids);
}

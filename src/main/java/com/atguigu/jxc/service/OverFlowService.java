package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverFlowService {

    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @param httpSession
     * @return
     */
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession httpSession);

    /**
     * 根据时间范围查询报溢单列表信息
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> goodList(Integer overflowListId);
}

package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierManageService {

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

    /**
     * 新增/修改供应商信息
     * @return
     */
    ServiceVO save(Supplier supplier);


    /**
     * 根据ID删除指定供应商
     * @param ids
     * @return
     */
    ServiceVO delete(String ids);
}

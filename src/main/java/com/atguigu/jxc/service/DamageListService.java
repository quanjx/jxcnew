package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListService {

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     *
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 新增报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession httpSession);

    /**
     * 查询报损单信息
     * @param damageListId
     * @return
     */
    Map<String, Object> goodsList(Integer damageListId);
}

package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class DamageListController {
    @Autowired
    private DamageListService damageListService;


    @PostMapping("/damageListGoods/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession httpSession){
        return damageListService.save(damageList,damageListGoodsStr,httpSession);
    }

    /**
     *报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/damageListGoods/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        return damageListService.list(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListService.goodsList(damageListId);
    }
}

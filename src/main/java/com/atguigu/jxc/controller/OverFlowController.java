package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class OverFlowController {
    @Autowired
    private OverFlowService overFlowService;

    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @param httpSession
     * @return
     */
    @PostMapping("/overflowListGoods/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession httpSession){
        return overFlowService.save(overflowList,overflowListGoodsStr,httpSession);
    }

    /**
     * 查询报溢单
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> list(String sTime, String eTime){
        return overFlowService.list(sTime,eTime);
    }

    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> goodList(Integer overflowListId){
        return overFlowService.goodList(overflowListId);
    }
}

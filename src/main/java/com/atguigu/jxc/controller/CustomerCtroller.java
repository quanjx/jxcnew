package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CustomerCtroller {
    @Autowired
    private CustomerService customerService;

    /**
     * 分页展示客户列表
     * @param page
     * @param rows
     * @param customerName
     * @return
     */

    @PostMapping("/customer/list")
    public Map<String,Object> list(Integer page, Integer rows, String customerName){
        return customerService.list(page,rows,customerName);
    }

    /**
     * 客户添加或修改
     * @param customer
     * @return
     */
    @PostMapping("customer/save")
    public ServiceVO save(Customer customer){
        return customerService.save(customer);
    }

    /**
     *删除客户
     * @param ids
     * @return
     */
    @PostMapping("/customer/delete")
    public ServiceVO delete(String ids){
        return customerService.delete(ids);
    }
}

package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SupplierManageController {
    @Autowired
    private SupplierManageService supplierManageService;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/supplier/list")
    public Map<String,Object> list(Integer page, Integer rows, String supplierName){

        return supplierManageService.list(page,rows,supplierName);

    }

    /**
     * 新增添加/修改供应商信息
     * @return
     */
    @PostMapping("supplier/save")
    public ServiceVO save(Supplier supplier){
        return supplierManageService.save(supplier);
    }

    /**
     * 根据ID删除指定供应商
     * @param ids
     * @return
     */
    @PostMapping("supplier/delete")
    public ServiceVO delete(String ids){
        return supplierManageService.delete(ids);
    }

}

package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {
    void insert(@Param("damageListGoods") DamageListGoods damageListGoods);

    /**
     * 获得报损单商品列表
     * @param damageListId
     * @return
     */
    List<DamageListGoods> getDamageGoodsList(@Param("damageListId") Integer damageListId);
}

package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierManageDao {

    /**
     * name条件查询供应商信息
     * @param offSet
     * @param rows
     * @param supplierName
     * @return
     */
    List<Supplier> getSupplierByName(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    /**
     * 获取supplier总数
     * @return
     */
    String getSupplierCount();
    /**
     * 更新供应商
     * @param supplierId
     */
    Integer updateById(@Param("supplierId") Integer supplierId, @Param("supplierName") String supplierName,
                       @Param("address") String address, @Param("remarks") String remarks, @Param("contacts") String contacts,
                       @Param("phoneNumber") String phoneNumber);


    Integer add(@Param("supplier") Supplier supplier);

    /**
     * 根据ID删除指定供应商
     * @param ids
     * @return
     */
    Integer deleteById(@Param("ids") String ids);
}

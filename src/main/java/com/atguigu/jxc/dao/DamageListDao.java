package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {

    /**
     * 根据时间查询报损单
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> getListByTime(@Param("sTime") String sTime, @Param("eTime") String eTime);

    /**
     *
     * @param damageList
     */
    void addDamageList(DamageList damageList);
}

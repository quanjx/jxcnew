package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

public interface UnitDao {
    /**
     * 查询所有商品单位
     * @return
     */
    List<Unit> getUnitList();
}

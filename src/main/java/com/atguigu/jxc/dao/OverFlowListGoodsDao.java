package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverFlowListGoodsDao {

    List<OverflowListGoods> getOverFlowGoodsList(@Param("overflowListId") Integer overflowListId);


    /**
     * 新增t_overflow_list_goods表
     * @param overflowGoods
     */
    void insert(@Param("overflowGoods") OverflowListGoods overflowGoods);
}

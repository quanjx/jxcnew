package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverFlowDao {
    /**
     * 新增OverflowList信息
     * @param overflowList
     */
    void addOverflowList(OverflowList overflowList);

    /**
     * 通过时间范围查询报溢单列表信息
     * @param sTime
     * @param eTime
     * @return
     */
    List<OverflowList> getListByTime(@Param("sTime") String sTime,@Param("eTime") String eTime);
}

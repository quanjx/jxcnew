package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    /**
     * 根据姓名进行模糊条件查询用户列表
     * @param offSet
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> getListByName(@Param("offSet") int offSet, @Param("rows") Integer rows,
                                 @Param("customerName") String customerName);

    /**
     * 汇总客户列表总数
     * @return
     */
    String getCustomerCount();

    /**
     * 更新客户信息
     * @param customerId
     * @param customer
     * @return
     */
    Integer updateById(@Param("customerId") Integer customerId, @Param("customer") Customer customer);


    Integer add(@Param("customer") Customer customer);

    /**
     * 删除客户信息
     * @param ids
     */
    void deleteById(@Param("ids") String ids);
}

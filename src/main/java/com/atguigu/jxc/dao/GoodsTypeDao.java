package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    /**
     * 查询商品分类的类型，父节点还是子节点
     * @param goodsTypeId
     * @return
     */
    Integer getTypeState(@Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 通过父节点找到其子节点的商品类型ID集合
     * @param goodsTypeId
     * @return
     */
    List<GoodsType> getGoodsChildrenTypeId(@Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 新增叶子结点
     */
    void addCategory(@Param("goodsTypeName") String goodsTypeName,@Param("pId") Integer pId);

    /**
     * 检查当前新增节点是否为父亲/叶子结点
     * @return
     */
    GoodsType checkGoodsTypeSta(@Param("pId") Integer pId);

    /**
     * 更新叶子结点为父亲节点
     * @param pId
     */
    void setGoodsTypeSta(@Param("pId") Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     */
    void delete(@Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 更新父亲节点为叶子结点
     * @param goodsTypeId
     */
    void setGoodsTypeSta0(@Param("goodsTypeId") Integer goodsTypeId);
}

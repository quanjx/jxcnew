package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    /**
     * 首页库存商品列表查询
     * @param offSet
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getGoodsListByCnameOrGType(@Param("offSet") int offSet, @Param("rows") Integer rows,
                                           @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 获得总商品数
     * @return
     */
    String getGoodsCount();

    /**
     * 查询商品信息（可以根据分类、名称查询）
     * @param offset
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getGoodList(@Param("offset") int offset,@Param("rows") Integer rows,@Param("goodsName") String goodsName
            ,@Param("goodsTypeId") Integer goodsTypeId);
    List<Goods> getGoodList(@Param("goodsTypeId") Integer goodsTypeId);

    //查询商品集合，用来判断没有关联商品的分类，删除分类的时候会用到
    List<Goods> getGoodListByTypeId(@Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 更新商品
     */
    void updateById(@Param("goods") Goods goods);

    /**
     * 添加商品
     * @param goods
     */
    void add(@Param("goods") Goods goods);

    /**
     * 删除指定商品
     * @param goodsId
     */
    void deleteById(@Param("goodsId") Integer goodsId);

    /**
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param nameOrCode
     * @return
     */
    List<Goods> getNoInventoryGoodList(@Param("offSet") int offSet,@Param("rows") Integer rows,
                                       @Param("nameOrCode") String nameOrCode);

    /**
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param offSet
     * @param rows
     * @param nameOrCode
     * @return
     */
    List<Goods> getHasInventoryGoodList(@Param("offSet")int offSet,@Param("rows") Integer rows,
                                        @Param("nameOrCode")String nameOrCode);

    /**
     * 更新初期库存
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void saveInventoryQuantity(@Param("goodsId")Integer goodsId, @Param("inventoryQuantity")Integer inventoryQuantity,
                               @Param("purchasingPrice") double purchasingPrice);

    /**
     * 获得商品库存状态
     * @param goodsId
     * @return
     */
    Integer getGoodsState(@Param("goodsId") Integer goodsId);

    /**
     * 删除状态为0的商品库存。更新商品库存为0
     * @param goodsId
     */
    void updateGoodsStock(@Param("goodsId") Integer goodsId);

    /**
     * 获取库存预警商品列表
     * @return
     */
    List<Goods> getaAlarmGoodsList();
}
